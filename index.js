const path = require('path');
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');
const flash = require('connect-flash');
const routes = require('./app/routes/routes');
const model = require('./app/model/model');
const helper = require('./app/services/helper');
const cors = require('cors');
const mysql = require('mysql');
const passport = require('passport');
const session = require('express-session');
const LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
const isProd = true;
exports.url = isProd ? "https://test-web.envspotjo.com" : "http://localhost:4200";

const app = module.exports = express();

try {
    let con;

    if (isProd) {
        con = mysql.createConnection({
            host: "spotwebdb.cuyej4coqxfs.eu-west-1.rds.amazonaws.com",
            user: "admin",
            password: "admin1234",
            database: "spotjodb_v2_11_02_2021"
        });
    } else {
        con = mysql.createConnection({
            host: "spotwebdb.cuyej4coqxfs.eu-west-1.rds.amazonaws.com",
            user: "admin",
            password: "admin1234",
            database: "spotjodb_v2_11_02_2021"
        });
    }
    
    con.connect((err) => {
        if (err) {
           
        } else {
            console.log("DB Connected!");
            helper.sqlCon = con;
            model.createTable();
        }
    });
} catch(err) {
    console.log(err);
}

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors());
app.use(bodyParser.json({ limit: '100mb' }));
app.use(cookieParser());
app.use(methodOverride());
app.use(flash());

app.use(session({
    secret: 'abcdefg',
    resave: true,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user, cb) => {
  cb(null, user);
});

passport.deserializeUser((user, cb) => {
  cb(null, user);
});


app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-with, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

if(process.env.NODE_ENV == 'developement') {
    app.use(express.static(path.join(__dirname, 'public')));
} else {
    app.use(express.static(path.join(__dirname, 'public'), {
        maxAge: '7d'
    }));
}


passport.use(new LinkedInStrategy({
  clientID: "78i6ymg0ykb26n",
  clientSecret: "gvUZbP64PHyLcc49",
  // callbackURL: "http://192.168.0.115:8091/linkedin-callback",
  callbackURL: "https://test-app.envspotjo.com/linkedin-callback",
  // callbackURL: "http://ec2-54-195-228-221.eu-west-1.compute.amazonaws.com:8091/linkedin-callback",
  scope: ['r_emailaddress', 'r_liteprofile'],
}, (accessToken, refreshToken, profile, done) => {
    process.nextTick(() => {
        return done(null, profile);
    });
}));


app.use('/', routes);
const server = http.createServer(app);
let allowedDomains = ['http://localhost:4200', 'http://localhost:8100', 'http://spotjo.wtipl.in', 'https://test-web.envspotjo.com'];

app.use(cors({
    origin: (origin, callback) => {
    // bypass the requests with no origin (like curl requests, mobile apps, etc )
    if (!origin) return callback(null, true);

    if (allowedDomains.indexOf(origin) === -1) {
    let msg = `This site ${origin} does not have an access. Only specific domains are allowed to access it.`;
    return callback(new Error(msg), false);
    }
    return callback(null, true);
    }
}));


const io = require('socket.io')(server, {
    cors: {
        origin: allowedDomains,
        methods: ["GET", "POST"], 
        transports: ['websocket', 'polling'],
        credentials: true,
    },
    allowEIO3: true
});

server.listen(process.env.PORT || 8091,()=> {
    console.log("Express server listening on port 8091");
});

const socketService = require('./app/controllers/socket-service');

io.on('connection', (socket) => {
    socketService.setSocket(socket);
});
name: ZAP Scan
    uses: zaproxy/action-baseline@v0.4.0
    with:
      target: 'https://test-app.envspotjo.com/api/webuser/role/get'

      sonar-scanner.bat -D"sonar.projectKey=asas" -D"sonar.sources=." -D"sonar.host.url=http://localhost:9000" -D"sonar.login=3193a942944fca96fce8dd2b132faf99e4206995"